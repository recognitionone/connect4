import React, { useState, useEffect } from 'react';
import dataProvider from './dataProvider';
import { v4 } from 'uuid';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles({
  container: {
    maxWidth: 360,
  },
  table: {
    // maxWidth: 80,
  },
  tableRow: {
    // maxWidth: 20,
    // maxHeight: 20,
    // color: 'white',
  },
  tableCell: {
    maxWidth: 20,
    // maxHeight: 20,
    // color: 'white',
  }
});

function CreateTable(props) {
  const classes = useStyles();
  const [bot, setBot] = useState('');
  const [botList, setBotList] = useState({});
  let someArray = [];

  useEffect(() => {
      dataProvider.getOne('bots', {})        
          .then((result) => {
              setBotList(result.data)
              
          })
          .catch(error => {
             
          })
  }, []);

  someArray = Object.values(botList)
  
  // const handleChange = event => {
  //   setBot(event.target.value);
  // };

  const rows = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    
  ]
    
    return (
    <TableContainer component={Paper} className={classes.container}>
      <Table className={classes.table} aria-label="simple table">
        <TableBody>
          {rows.map(row => (
            <TableRow key={v4()} className={classes.tableRow}>
              {row.map(cell => (
                <TableCell align="center" className={classes.tableCell}>{cell}</TableCell>
                ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default CreateTable;


