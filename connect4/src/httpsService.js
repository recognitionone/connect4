// Source: https://gist.github.com/lastguest/1fd181a9c9db0550a847#gistcomment-3062641
// Encoding an object as application/x-www-form-urlencoded
function toFormUrlEncoded(object) {
  return Object.entries(object)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

export default class HttpsService {  

  static async postData(url, form) {

      return fetch(url, {
        method: 'POST',
        mode: 'cors', 
        cache: 'no-cache',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: toFormUrlEncoded(form)
        
      })
      .then(response => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error(response.statusText);
          }
          return response.json();
      })
    }


  static getData(url) {
      return fetch(url, {
        method: 'GET',
        mode: 'cors', 
        cache: 'no-cache',
        // credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
          // 'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      })
      .then(response => {
        if (response.status >= 400) {
            throw new Error(response.statusText);
        }
        //jeżeli dostaję 403 to powinnam przechodzić do strony logowania
        return response.json();
      })
    }
    
  // static putData(url, form) {

  //   function getCookie(name) {
  //       let cookieValue = null;
  //       if (document.cookie && document.cookie !== '') {
  //           var cookies = document.cookie.split(';');
  //           for (var i = 0; i < cookies.length; i++) {
  //               var cookie = cookies[i].trim();
  //               if (cookie.substring(0, name.length + 1) === (name + '=')) {
  //                   cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
  //                   break;
  //               }
  //           }
  //       }
  //       return cookieValue;
  //   }
  //   let csrftoken = getCookie('csrftoken');

  //   return fetch(url, {
  //       method: 'PUT',
  //       mode: 'cors', 
  //       cache: 'no-cache',
  //       credentials: 'include',
  //       headers: {
  //         'X-CSRFToken': csrftoken,
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(form)
  //     })
  //     .then(response => {
  //         if (response.status >= 400) {
  //           throw new Error(response.statusText);
          
  //         }
  //         return response.json();
  //     })
  //   }

  //   static deleteData(url, form) {

  //     function getCookie(name) {
  //       let cookieValue = null;
  //       if (document.cookie && document.cookie !== '') {
  //           var cookies = document.cookie.split(';');
  //           for (var i = 0; i < cookies.length; i++) {
  //               var cookie = cookies[i].trim();
  //               if (cookie.substring(0, name.length + 1) === (name + '=')) {
  //                   cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
  //                   break;
  //               }
  //           }
  //       }
  //       return cookieValue;
  //     }
  //     let csrftoken = getCookie('csrftoken');


  //     return fetch(url, {
  //       method: 'DELETE',
  //       mode: 'cors', 
  //       cache: 'no-cache',
  //       credentials: 'include',
  //       headers: {
  //         'X-CSRFToken': csrftoken,
  //         'Content-Type': 'application/json',
  //       },        
  //     })
  //     .then(response => {
  //         if (response.status >= 400) {
  //             throw new Error(response.statusText);
  //         }
  //     })
  //   }  
}
