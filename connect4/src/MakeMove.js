import React, { useState, useEffect } from 'react';
import dataProvider from './dataProvider';


function MakeMove(props) {
  const [gameStatus, setGameStatus] = useState('');

  useEffect(() => {
      dataProvider.create('make_move', {data: {'game_id': ['abcdefghij'], 'move': 0}})        
          .then((result) => {
              setGameStatus(result.data)
          })
          .catch(error => {
          })
  }, []);

  console.log('make_move: ', gameStatus);
    
    return (
      <div >
        <p>hello gameStatus</p>
      </div>
  );
}

export default MakeMove;


