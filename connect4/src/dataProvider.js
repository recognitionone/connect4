import HttpsService from './httpsService';
// import { baseUrl } from './utils/url';

const baseUrl = 'https://agutowski.com/connect4'

const dataProvider = {

    // getList: (resource, params) => {
    //   // console.log(resource, " getList params: ", params)                
    //   const localUrl = `${apiUrl}/${resource}/`;
    //   return HttpsService.getData(localUrl)
    //   .then(response => {
    //     return { 
    //         data: response,
    //         total: response.length 
    //       }
    //   })
    // },

    getOne: (resource, params ) => {
      return HttpsService.getData(`${baseUrl}/${resource}/`)
      .then(response => {
        return { 
            data: response,
          }
      })
    },

    create:     (resource, params) => {
      
      return HttpsService.postData(`${baseUrl}/${resource}/`, params.data )
      .then(response => {
        return { 
            data: response,
           }
      })
    },

    // update:     (resource, params) => {
    //   // console.log('trying to update: ', resource, "update: ", params)
    //   if (params.choiceId) {
    //     return HttpsService.putData(`${apiUrl}/${resource}/${params.choiceId}/`, params.data )
    //     .then(response => {
    //       return { 
    //           data: response,
    //           total: response.length 
    //         }
    //     })
    //   } else {
    //     return HttpsService.putData(`${apiUrl}/estimates/${params.id}/`, params.data )
    //     .then(response => {
    //       return { 
    //           data: response,
    //           total: response.length 
    //         }
    //     })
    //   }
    // },

    // updateMany: (resource, params) => {
    //   return HttpsService.putData(`${apiUrl}/${resource}/${params.ids}/`)
    // },

    // delete:     (resource, params) => {
    //   return HttpsService.deleteData(`${apiUrl}/estimates/${params.id}/`)
    // },

    // deleteMany: (resource, params) => {
    //   const promises = params.ids.map(item => HttpsService.deleteData(`${apiUrl}/estimates/${item}/`))
    //   return Promise.all(promises)
    //   .then(response => {
    //     return { 
    //         data: params.ids,
    //       }
    //     })
    // },
}

export default dataProvider;
