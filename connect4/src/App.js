import React from 'react';
import './App.css';
import GetBotList from './GetBotList';
import CreateTable from './CreateTable';
import StartGame from './StartGame';
import MakeMove from './MakeMove';


function App() {
  return (
    <div className="App">
      <header className="App-header">   
      	<StartGame />
      	<MakeMove />     
        <GetBotList />
        <CreateTable />
      </header>
    </div>
  );
}

export default App;
