import React, { useState, useEffect } from 'react';
import dataProvider from './dataProvider';
import { v4 } from 'uuid';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(3),
    minWidth: 120,
    backgroundColor: 'white',
    width: '140%',
    // padding: 20,

  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

function GetBotList(props) {
  const classes = useStyles();
  const [bot, setBot] = useState('');
  const [botList, setBotList] = useState({});
  let someArray = [];

  useEffect(() => {
      dataProvider.getOne('bots', {})        
          .then((result) => {
              setBotList(result.data)
              
          })
          .catch(error => {
             
          })
  }, []);

  someArray = Object.values(botList)
  
  const handleChange = event => {
    setBot(event.target.value);
  };
    
    return (
      <div >
        <FormControl variant="filled" className={classes.formControl} component={Paper}>
        <InputLabel id="demo-simple-select-filled-label">Choose your bot</InputLabel>
        <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          value={bot}
          onChange={handleChange}
        >
          {someArray.map(entry => (
             <MenuItem value={entry} key={v4()}>{entry}</MenuItem>
           ))}
        </Select>
      </FormControl>
    </div>
  );
}

export default GetBotList;


