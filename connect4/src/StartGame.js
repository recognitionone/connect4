import React, { useState, useEffect } from 'react';
import dataProvider from './dataProvider';


function StartGame(props) {
  const [gameStatus, setGameStatus] = useState('');

  useEffect(() => {
      dataProvider.create('create_game', {data: {'bot': 'level0'}})        
          .then((result) => {
              setGameStatus(result.data)
          })
          .catch(error => {
          })
  }, []);

  console.log('create_game: ', gameStatus);
    
    return (
      <div >
        <p>hello gameStatus</p>
      </div>
  );
}

export default StartGame;


